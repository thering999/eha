<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use flareui\assets\FlareuiAsset;

FlareuiAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@flareui/dist');
?>
<!-- header &&  horizontal navbar -->
<nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
    <div class="nav-top flex-grow-1">
        <div class="container d-flex flex-row h-100">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top">
                <a style="color: white;" class="navbar-brand brand-logo" href="<?php echo \yii\helpers\Url::base(true); ?>"><img height="200px" src=<?php echo $directoryAsset."/images/Logo-EHA-thai.jpg"?>> ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม</a>
                <a class="navbar-brand brand-logo-mini" href="index.html"></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <form class="search-field" action="#">
                    <div class="form-group mb-0">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="search">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                        </div>
                    </div>
                </form>
                <ul class="navbar-nav navbar-nav-right mr-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                            <i class="icon-bell"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                            <a class="dropdown-item py-3">
                                <p class="mb-0 font-weight-medium float-left">You have 4 new notifications
                                </p>
                                <span class="badge badge-pill badge-inverse-info float-right">View all</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-inverse-success">
                                        <i class="icon-exclamation mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Application Error</h6>
                                    <p class="font-weight-light small-text mb-0">
                                        Just now
                                    </p>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-inverse-warning">
                                        <i class="icon-bubble mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Settings</h6>
                                    <p class="font-weight-light small-text mb-0">
                                        Private message
                                    </p>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-inverse-info">
                                        <i class="icon-user-following mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-normal text-dark mb-1">New user registration</h6>
                                    <p class="font-weight-light small-text mb-0">
                                        2 days ago
                                    </p>
                                </div>
                            </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a style="color: white;" id="notificationDropdown" href="#" data-toggle="dropdown">
                            <i class="icon-user"></i> <?php echo Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->getPublicIdentity(); ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                            <?php if(Yii::$app->user->isGuest):?>
                                <a href="<?php echo \yii\helpers\Url::to(['user/default/index'])?>" class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-inverse-success">
                                            <i class="icon-login mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-normal text-dark mb-1">เข้าสู่ระบบ</h6>
                                        <p class="font-weight-light small-text mb-0">
                                        </p>
                                    </div>
                                </a>
                            <?php endif;?>
                            <?php if(!Yii::$app->user->isGuest):?>
                                <a href="<?php echo \yii\helpers\Url::to(["user/default/index"])?>" class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-inverse-success">
                                        <i class="icon-user mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-normal text-dark mb-1">บัญชีผู้ใช้</h6>
                                    <p class="font-weight-light small-text mb-0">
                                    </p>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a data-method="POST" href="<?php echo \yii\helpers\Url::to(['/user/sign-in/logout']); ?>" class="dropdown-item preview-item">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-inverse-danger">
                                        <i class="icon-logout mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-normal text-dark mb-1">ออกจากระบบ</h6>
                                    <p class="font-weight-light small-text mb-0">
                                    </p>
                                </div>
                            </a>
                            <?php endif;?>
                        </div>
                    </li>

                </ul>
                <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu text-white"></span>
                </button>
            </div>
        </div>
    </div>
    <!-- Nav Bar Button -->
    <div class="nav-bottom">
    <div class="container">
    <ul class="nav page-navigation">
        <li class="nav-item active">
            <a href="<?php echo \yii\helpers\Url::to(['/site/index']);?>" class="nav-link"><i class="link-icon icon-home"></i><span class="menu-title">หน้าหลัก</span></a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-screen-desktop"></i><span class="menu-title">รู้จัก EHA</span></a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-list"></i><span class="menu-title">คู่มือ เอกสาร</span></a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">ประเมิน EHA</span></a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">อปท.ที่ผ่านการประเมิน</span></a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">รายชื่อ อปท.ทั่วประเทศ</span></a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">การประชุม</span></a>
        </li>
    </ul>
    </div>
    </div>
</nav>