<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
?>

<?php $this->beginContent('@flareui/views/layouts/base.php')?>

    <div class="container-scroller">

        <?=$this->render('_header.php', []) ?>

        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel">
                <div class="content-wrapper">
                    <?php echo $content ?>
                </div>

                <?= $this->render('_footer.php', []) ?>

            </div>
        </div>
    </div>
<?php $this->endContent() ?>