<?php
namespace flareui\assets;

use yii\web\AssetBundle;

class FlareuiAsset extends AssetBundle
{

    public $sourcePath = '@flareui/dist';
    public $css = [
        'vendors/iconfonts/simple-line-icon/css/simple-line-icons.css',
        'vendors/css/vendor.bundle.base.css',
        'vendors/css/vendor.bundle.addons.css',
        'css/style.css',
        'images/favicon.png'
    ];

    public $js = [
        'vendors/js/vendor.bundle.base.js',
        'vendors/js/vendor.bundle.addons.js',
        'js/dashboard.js',
        'js/todolist.js',
        'js/template.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}