<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('frontend', 'User Settings')
?>

<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?php echo Html::encode($this->title) ?></h4>

                <p class="card-description">
                </p>

                <div class="user-profile-form">

                    <?php $form = ActiveForm::begin(); ?>


                    <?php echo $form->field($model->getModel('profile'), 'picture')->widget(
                        Upload::class,
                        [
                            'url' => ['avatar-upload']
                        ]
                    )?>

                    <?php echo $form->field($model->getModel('profile'), 'firstname')->textInput(['maxlength' => 255]) ?>


                    <?php echo $form->field($model->getModel('profile'), 'lastname')->textInput(['maxlength' => 255]) ?>


                    <?php echo $form->field($model->getModel('profile'), 'gender')->dropDownlist([
                        \common\models\UserProfile::GENDER_FEMALE => Yii::t('frontend', 'Female'),
                        \common\models\UserProfile::GENDER_MALE => Yii::t('frontend', 'Male')
                    ], ['prompt' => '']) ?>


                    <?php echo $form->field($model->getModel('account'), 'username') ?>

                    <?php echo $form->field($model->getModel('account'), 'email') ?>

                    <?php echo $form->field($model->getModel('account'), 'password')->passwordInput() ?>

                    <?php echo $form->field($model->getModel('account'), 'password_confirm')->passwordInput() ?>

                    <div class="form-group">
                        <?php echo Html::submitButton(Yii::t('frontend', 'Update'), ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
